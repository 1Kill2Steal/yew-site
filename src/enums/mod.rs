#[derive(Hash, PartialEq, Eq, Clone, Copy, Debug)]
pub enum Socials {
    Twitter,
    GitHub,
    GitLab,
    Codeberg,
    Discord,
}
